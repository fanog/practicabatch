package com.bbva.qwai.batch.processor;

import org.springframework.batch.item.ItemProcessor;

public class HelloWorldItemProcessor implements ItemProcessor<String, String> {
	

public String process(String input) throws Exception{
		//arg0 = "Hello|World
		// lo transformamos en Hello World
        String[] message = input.split(",");
        return message[0] +" " + message[1];
    }

}
